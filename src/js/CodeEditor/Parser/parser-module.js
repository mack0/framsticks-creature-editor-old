/**
 *  A module representing Parser.
 *  @namespace CodeEditor/Parser
 */
define(
    'CodeEditor/Parser/parser-module',
    ['Model/Body', 'Configuration/configuration-module', 'Utils/utils-module',],
    function ParserModule (Body, config, Utils) {
        "use strict";

        /////////////////// API
        /**
         * @class
         * @memberof Parser
         */
        var Parser = {
            get annotations () {
                return annotations;
            },
            set annotations (ann) {
                annotations = ann;
            },
            getLastRefNumber: getLastRefNumber,
            parseCode: parseCode,
            parseLine: parseLine
        };

        /////////////////// Data store
        var REG_COMMENT             = /^(\/\/)|#/,
            REG_CONNECTION          = /^c:/,
            REG_JOINT               = /^j:/,
            REG_MODEL_PROPERTIES    = /^m:/,
            REG_NEURON              = /^n:/,
            REG_PART                = /^p:/,
            refNumberComment        = 0,
            refNumberConnection     = 0,
            refNumberJoint          = 0,
            refNumberModelProperties= 0,
            refNumberNeuron         = 0,
            refNumberPart           = 0,
            annotations             = [];

        /////////////////// Methods
        function getLastRefNumber (type) {
            switch(type) {
                case '#':
                    return refNumberComment;
                case 'c':
                    return refNumberConnection;
                case 'j':
                    return refNumberJoint;
                case 'm':
                    return refNumberModelProperties;
                case 'n':
                    return refNumberNeuron;
                case 'p':
                    return refNumberPart;
                default:
                    return 0;
            }
        }

        function parseCode (lines, editor) {
            var line, modelAnnotations = [],
                i;

            refNumberComment        = 0;
            refNumberConnection     = 0;
            refNumberJoint          = 0;
            refNumberModelProperties= 0;
            refNumberNeuron         = 0;
            refNumberPart           = 0;
            annotations             = [];

            if (!config.mode.embed) {
                editor.getSession().clearAnnotations();
            }

            for (i = 0; i < lines.length; i++) {
                if (REG_COMMENT.test(lines[i])) {
                    line = lines[i];                        // don't remove whitespaces for comments
                } else {
                    line = lines[i].replace(/ /g, '');      // remove whitespaces
                }

                parseLine(i, line);
            }

            _analyzeConnectednessOfParts(annotations);
            _analyzeCyclicJoints(annotations);
            _isPartsShapeMixed(annotations);

            if (!config.mode.embed) {
                for (i = 0; i < Body.objects.length; i++) {
                    if (Body.objects[i].type !== '#') {
                        modelAnnotations = Body.objects[i].getModelAnnotations();

                        for (var j = 0; j < modelAnnotations.length; j++) {
                            annotations.push(modelAnnotations[j]);
                        }
                    }
                }
            }

            if (annotations.length > 0 && !config.mode.embed) {
                setTimeout(function () {
                    editor.getSession().setAnnotations(annotations);
                }, 300);
            }
        }

        function _analyzeConnectednessOfParts(annotations) {
            var partsNumbers = [],
                partsConnectedByJoints = [],
                part, i;

            for (i = 0; i < Body.objects.length; i++) {
                if (Body.objects[i].type === 'p') {
                    partsNumbers.push(Body.objects[i].refNumber);
                }
                if (Body.objects[i].type === 'j') {
                    partsConnectedByJoints.push(Body.objects[i].model.p1.value);
                    partsConnectedByJoints.push(Body.objects[i].model.p2.value);
                }
            }

            if (partsNumbers.length > 1) {
                for (i = 0; i < partsNumbers.length; i++) {
                    if (partsConnectedByJoints.indexOf(partsNumbers[i]) === -1) {
                        part = Body.getObjectByRefNumber(partsNumbers[i], 'p');

                        annotations.push({
                            row: part.line,
                            column: 1,
                            text: 'part #' + part.refNumber + ' is not connected with any other part',
                            type: 'error'
                        });
                    }
                }
            }
        }

        function _isPartsShapeMixed(annotations) {
            var part,
                lastPartInLine = null,
                shValues = {
                    0: 0,
                    1: 0,
                    2: 0,
                    3: 0
                };

            for (var i = 0; i < Body.objects.length; i++) {
                if (Body.objects[i].type === 'p') {
                    part = Body.objects[i];

                    shValues[parseInt(part.model.sh.value)]++;
                    lastPartInLine = part.line;
                }
            }
            
            if (shValues[0] > 0 && (shValues[1] > 0 || shValues[2] > 0 || shValues[3] > 0)) {
                annotations.push({
                    row: lastPartInLine,
                    column: 1,
                    text: 'Inconsistent part shapes (mixed old and new shapes).\n[Number of parts]: [' + shValues[0] + '] with sh=0 and [' + (shValues[1] + shValues[2] + shValues[3]) + '] with sh>0.',
                    type: 'warning'
                });
            }
        }

        function _analyzeCyclicJoints(annotations) {
            var i, joint, asP1;

            for (i = 0; i < Body.objects.length; i++) {
                if (Body.objects[i].type === 'j') {
                    Body.objects[i].deltaLookup = undefined;
                }
            }

            for (i = 0; i < Body.objects.length; i++) {
                if (Body.objects[i].type === 'j') {
                    joint = Body.objects[i];

                    if (!joint.model.dx.ommit || !joint.model.dy.ommit || !joint.model.dz.ommit) {
                        joint.deltaLookup = [joint.model.p1.value, joint.model.p2.value];

                        asP1 = Body.getJointsByPartRefNumberWithDistinction(joint.model.p2.value).partAsP1;

                        if (asP1.length > 0) {
                            for (var j = 0; j < asP1.length; j++) {
                                if (typeof asP1[j].deltaLookup !== 'undefined') {
                                    joint.deltaLookup = joint.deltaLookup.concat(asP1[j].deltaLookup);

                                    if (Utils.hasDuplicates(joint.deltaLookup)) {
                                        annotations.push({
                                            row: joint.line,
                                            column: 1,
                                            text: 'delta joint cycle detected at joint#' + joint.refNumber,
                                            type: 'error'
                                        });
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        function parseLine (i, line) {
            var settings;

            settings = {
                line: i
            };

            if (line === '' || REG_COMMENT.test(line)) {
                settings.type = '#';
                settings.value = line;
                settings.refNumber = refNumberComment++;
            } else if (REG_CONNECTION.test(line)) {
                settings.type = 'c';
                settings.refNumber = refNumberConnection++;
            } else if (REG_JOINT.test(line)) {
                settings.type = 'j';
                settings.refNumber = refNumberJoint++;
            } else if (REG_MODEL_PROPERTIES.test(line)) {
                settings.type = 'm';
                settings.refNumber = refNumberModelProperties++;
            } else if (REG_NEURON.test(line)) {
                settings.type = 'n';
                settings.refNumber = refNumberNeuron++;
            } else if (REG_PART.test(line)) {
                settings.type = 'p';
                settings.refNumber = refNumberPart++;
            } else if (line.length > 1 && !config.mode.embed) {
                annotations.push({
                    row: i,
                    column: 1,
                    text: 'Unrecognized object type',
                    type: 'error'
                });
            }

            settings.code = line;

            Body.manageObjects(settings);
        }

        return Parser;
    }
);