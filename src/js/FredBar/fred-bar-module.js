/**
 *  A module representing FredBar.
 *  @namespace FredBar
 */
define(
    'FredBar/fred-bar-module',
    [
        'jquery',
        'Editor3d/Scene/Camera/camera-module',
        'CodeEditor/code-editor-module',
        'Configuration/configuration-module',
        'Editor3d/Scene/Controls/controls-module',
        'Model/Body',
        'Embed/embed-module',

        'jqueryui'
    ],
    function FredBarModule ($, Camera, CodeEditor, config, Controls, Body, Embed) {
        "use strict";

        /////////////////// API
        /**
         * @class
         * @memberof ModelBody
         */
        var FredBar = {
            // regular methods
            createModel: createModel,
            createNewModel: createNewModel,
            getModels: getModels,
            getModelById: getModelById,
            exportEmbed: exportEmbed,
            init: init,
            isFSSupportedWithAlert: isFSSupportedWithAlert,
            loadModel: loadModel,
            loadModelFromDB: loadModelFromDB,
            modifyModel: modifyModel,
            openFile: openFile,
            openModelDialog: openModelDialog,
            prepareModelsList: prepareModelsList,
            saveAsHTML: saveAsHTML,
            saveModel: saveModel,
            saveFile: saveFile
        };

        /////////////////// Data store
        var models = [],
            modelsDialog,
            selectedGenotype = {},
            genotypesStore;

        /////////////////// Private Methods
        function _onFileInputChange (evt) {
            var f = evt.target.files[0], r,
                name = '', genotypes;

            if (f && f.type.match('text/plain')) {
                name = f.name.substr(0, f.name.lastIndexOf('.')) || f.name;
                r = new FileReader();
                r.onload = function (e) {
                    var contents = e.target.result;

                    genotypes = searchForGenotypes(contents);

                    selectGenotype(genotypes, f.name);
                };
                r.readAsText(f);
                $('#file-input').val('');
            }
        }

        function setModelFromFile (contents, name) {
            CodeEditor.editor.setValue(CodeEditor.editor.defaultCode);
            Body.removeAllObjects();

            CodeEditor.editor.setValue(contents);
            CodeEditor.editor.clearSelection();

            //$('#view-reset').trigger('click');
            $('#model-name').val(name);
        }

        function selectGenotype (genotypes, name) {
            var html = '';

            genotypesStore = genotypes;

            if (genotypes.length === 1) {
                selectedGenotype = genotypes[0];
                setModelFromFile(genotypes[0].code, genotypes[0].name);
                genotypesStore = [];
            } else {
                if (genotypes.length === 0) {
                    $('#models-in-file-list').hide();
                    $('#models-in-file-empty-list').show();
                } else if (genotypes.length > 1) {
                    for (var i = 0, len = genotypes.length; i < len; i++) {
                        html += '<li data-model-id="'+i+'">' + genotypes[i].name + '</li>';
                    }
                    $('#models-in-file-empty-list').hide();
                    $('#models-in-file-list').html(html).show();
                }

                $("#models-in-file").dialog({
                    modal: true,
                    width: 500,
                    title: 'Models in file ' + name,
                    buttons: {
                        Cancel: function () {
                            $(this).dialog("close");
                        }
                    }
                });
            }
        }

        function searchForGenotypes (contents) {
            var lines = contents.split('\n'),
                line,
                genotype = {
                    code: '',
                    info: ''
                },
                genotypes = [],
                isCodeStream = false,
                isInfoStream = false;


            for (var i = 0, len = lines.length; i < len; i++) {
                line = lines[i];

                if (line === 'genotype:~' && typeof lines[i+1] !== 'undefined' && lines[i+1].substring(0,3) === '//0') {
                    genotype.code = '';
                    isCodeStream = true;
                } else if (line === 'info:~') {
                    genotype.info = '';
                    isInfoStream = true;
                } else if (isCodeStream && line[line.length - 1] === '~') {
                    genotype.code += line.substring(0, line.length - 1);
                    isCodeStream = false;
                } else if (isInfoStream && line[line.length - 1] === '~') {
                    genotype.info += line.substring(0, line.length - 1);
                    isInfoStream = false;
                } else {
                    if (isCodeStream && line !== '') {
                        genotype.code += line + '\n';
                    } else if (isInfoStream && line !== '') {
                        genotype.info += line + '\n';
                    } else {
                        if (line.substring(0,4) === 'org:') {
                            if (line.length > 4) {
                                genotype.org = line.substring(4,line.length);
                            } else {
                                genotype.org = '';
                            }
                        } else if (line.substring(0,5) === 'name:') {
                            if (line.length > 5) {
                                genotype.name = line.substring(5,line.length);
                            } else {
                                genotype.name = '';
                            }
                        } else if (line.substring(0,5) === 'info:' && line[line.length-1] !== '~') {
                            if (line.length > 5) {
                                genotype.info = line.substring(5,line.length);
                            } else {
                                genotype.info  = '';
                            }
                        }
                    }
                }

                if (!isCodeStream && !isInfoStream && line === '' && typeof genotype.name !== 'undefined' && genotype.code !== '') {
                    genotypes.push(genotype);
                    genotype = {
                        code: '',
                        info: ''
                    };
                }
            }

            return genotypes;
        }

        function createModel (model) {
            $.ajax({
                url: '/api/models',
                method: 'POST',
                data: model,
                dataType: 'json',
                error: function () {
                    var $modal = $("#modal-info");

                    $modal.attr('title', 'Failure');

                    $('#modal-info-content').html("Ups... something gone wrong");
                    $modal.dialog({
                        modal: true,
                        width: 500,
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        }
                    });
                },
                success: function (data) {
                    var $modal = $("#modal-info");

                    $modal.attr('title', 'Success!');

                    $('#modal-info-content').html("You've just successfully created and save model!");
                    $modal.dialog({
                        modal: true,
                        width: 500,
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        }
                    });

                    window.location.href = window.location.origin + '/#/' + data._id;
                }
            });
        }

        function createNewModel () {
            window.location.href = window.location.origin + '/#/';
            window.location.reload();
        }

        function getModels (cb) {
            $.get('/api/models', function (data) {
                models = data;

                $('#models-list').html(FredBar.prepareModelsList(models));
                cb();
            });
        }

        function getModelById (id) {
            for (var i = 0, len = models.length; i < len; i++) {
                if (models[i]._id === id) {
                    return models[i];
                }
            }

            return null;
        }

        function exportEmbed (ev) {
            var $modal = $("#modal-export");

            ev.preventDefault();

            Controls.controls.enabled = false;

            $('#embedEditorURL').val(config.values.embedEditorUrlSameDomain);
            $('#modal-export-content').val(Embed.generateHTML(CodeEditor.editor.getValue()));
            $modal.dialog({
                modal: true,
                width: 500,
                buttons: {
                    Close: function () {
                        Controls.controls.enabled = true;
                        $(this).dialog("close");
                    }
                }
            });
        }

        function init () {
            if (!config.mode.embed) {
                $('#new-model').on('click', function (ev) {
                    ev.preventDefault();
                    if (confirm("Are you sure? If you did not save your current model you will lose all changes.")) {
                        FredBar.createNewModel();
                    }
                });

                $('#model-description-show-more').on('click', function () {
                    $('#model-desc-author').val(selectedGenotype.author);
                    $('#model-desc-info').val(selectedGenotype.info);

                    $("#model-description-dialog").dialog({
                        modal: true,
                        width: 500,
                        buttons: {
                            Ok: function () {
                                selectedGenotype.author = $('#model-desc-author').val();
                                selectedGenotype.info = $('#model-desc-info').val();
                                $(this).dialog("close");
                            }
                        }
                    });
                });

                $('#open-database').on('click', function () {
                    FredBar.getModels(FredBar.openModelDialog);
                });

                $('#open-file').on('click', FredBar.openFile);
                $('#save-file').on('click', FredBar.saveFile);
                $('#export-embed').on('click', FredBar.exportEmbed);
                $('#export-save-html').on('click', FredBar.saveAsHTML);

                $('#file-input').on('change', _onFileInputChange);

                $('#save-in-database').on('click', function () {
                    var modelId = window.location.hash.replace('#/', '');

                    FredBar.saveModel(modelId);
                });

                $('#models-list').on('click', 'a.js-model-link', function () {
                    var modelId = $(this).data('modelId');

                    if (typeof modelsDialog !== 'undefined' && modelId !== null && typeof modelId !== 'undefined') {
                        FredBar.loadModelFromDB(modelId, function () {
                            modelsDialog.dialog('close');
                        });
                    }
                });

                $('#models-in-file-list').on('click', 'li', function () {
                    var modelId = $(this).data('modelId');

                    setModelFromFile(genotypesStore[modelId].code, genotypesStore[modelId].name);

                    selectedGenotype = genotypesStore[modelId];
                    genotypesStore = [];
                    $("#models-in-file").dialog("close");
                });

                $('.js-embed-controls input').on('change', function () {
                    $('#modal-export-content').val(Embed.generateHTML(CodeEditor.editor.getValue()));
                });

                $('#model-desc-author').on('change', function () {
                    selectedGenotype.author = $(this).val();
                });

                $('#model-desc-info').on('change', function () {
                    selectedGenotype.info = $(this).val();
                });

                $('#embedCameraCurrent').on('click', function () {
                    $('#embedCameraX').val(Math.round(Camera.camera.position.x * 100) / 100);
                    $('#embedCameraY').val(Math.round(Camera.camera.position.y * 100) / 100);
                    $('#embedCameraZ').val(Math.round(Camera.camera.position.z * 100) / 100);
                    $('#embedTargetX').val(Math.round(Controls.controls.target.x * 100) / 100);
                    $('#embedTargetY').val(Math.round(Controls.controls.target.y * 100) / 100);
                    $('#embedTargetZ').val(Math.round(Controls.controls.target.z * 100) / 100);
                    $('#modal-export-content').val(Embed.generateHTML(CodeEditor.editor.getValue()));
                });

                $('#embedCameraReset').on('click', function () {
                    $('#embedCameraX').val(config.values.camera.position.x);
                    $('#embedCameraY').val(config.values.camera.position.y);
                    $('#embedCameraZ').val(config.values.camera.position.z);
                    $('#embedTargetX').val(0);
                    $('#embedTargetY').val(0);
                    $('#embedTargetZ').val(0);
                    $('#modal-export-content').val(Embed.generateHTML(CodeEditor.editor.getValue()));
                });

                $('#embedDomainCheckboxes .embedDomain').on('click', function (ev) {
                    config.values.embedUseDomain = $(this).val();
                    $('#embedEditorURL').val(config.values[config.values.embedUseDomain]);
                    $('#modal-export-content').val(Embed.generateHTML(CodeEditor.editor.getValue()));
                });

                $('.dropdown').on('click', function () {
                    var dropdown = $(this);
                    dropdown.toggleClass('open');

                    var isOpen = dropdown.hasClass('open');

                    if (isOpen) {
                        setTimeout(function () {
                            $(window).on('click', function() {
                                dropdown.removeClass('open');
                                $(window).off('click');
                            });
                        }, 300)
                    } else {
                        $(window).off('click');
                    }
                });
            }
        }

        function isFSSupportedWithAlert () {
            if (window.File && window.FileReader && window.FileList && window.Blob) {
                return true;
            } else {
                alert('Opening from file or saving to the file feature is possible only in modern browsers. Use a new version of Chrome, Firefox or Safari.');
                return false;
            }
        }

        function loadModel (model) {
            window.location.href = window.location.origin + '/#/' + model._id;
            window.location.reload();
        }

        function loadModelFromDB (modelId, cb) {
            $.get('/api/models/' + modelId, function (model) {
                FredBar.loadModel(model);
                cb();
            });
        }

        function modifyModel(modelId, model) {
            $.ajax({
                url: '/api/models/' + modelId,
                method: 'PUT',
                data: model,
                dataType: 'json',
                error: function () {
                    var $modal = $("#modal-info");

                    $modal.attr('title', 'Failure');

                    $('#modal-info-content').html("Ups... something gone wrong");
                    $modal.dialog({
                        modal: true,
                        width: 500,
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        }
                    });
                },
                success: function () {
                    var $modal = $("#modal-info");

                    $modal.attr('title', 'Success!');

                    $('#modal-info-content').html("You've just successfully saved your current model!");
                    $modal.dialog({
                        modal: true,
                        width: 500,
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        }
                    });
                }
            });
        }

        function openFile (ev) {
            ev.preventDefault();
            if (FredBar.isFSSupportedWithAlert()) {
                $('#file-input').trigger('click');
            }
        }

        function openModelDialog () {
            modelsDialog = $("#models-database").dialog({
                modal: true,
                width: 500,
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }

        function prepareModelsList (models) {
            var html = '';

            for (var i = 0, len = models.length; i < len; i++) {
                html += '<li>';
                html += '<a href="#/' + models[i]._id + '" class="js-model-link" data-model-id="' + models[i]._id + '">' + (models[i].name || 'Untitled') + '</a>';
                html += '</li>';
            }

            return html;
        }

        function saveAsHTML () {
            var linkAnchor, value, textFileAsBlob;

            if (isFSSupportedWithAlert()) {
                linkAnchor = $('#export-html-anchor')[0];
                value = $('#modal-export-content').val();
                textFileAsBlob = new Blob([value], {type:'text/html'});

                if (linkAnchor) {
                    linkAnchor.download = $('#model-name').val();
                    linkAnchor.href = window.URL.createObjectURL(textFileAsBlob);

                    linkAnchor.click();
                }
            }
        }


        function saveFile (ev) {
            var linkAnchor, value = '', textFileAsBlob;

            ev.preventDefault();

            if (isFSSupportedWithAlert()) {
                linkAnchor = $('#save-file-anchor')[0];

                selectedGenotype.author = selectedGenotype.author || '';
                selectedGenotype.info = selectedGenotype.info || '';

                value += '\n';
                value += 'org:\n';
                value += 'name:' + $('#model-name').val() + '\n';
                value += 'genotype:~' + '\n';
                value += CodeEditor.editor.getValue();
                value += '\n' + '~' + '\n';
                value += 'info:~' + '\n';
                value += 'Author: ' + selectedGenotype.author + '\n';
                value += selectedGenotype.info + '~' + '\n';
                textFileAsBlob = new Blob([value], {type:'text/plain'});

                if (linkAnchor) {
                    linkAnchor.download = $('#model-name').val() + '.gen';
                    linkAnchor.href = window.URL.createObjectURL(textFileAsBlob);

                    linkAnchor.click();
                }
            }
        }

        function saveModel (modelId) {
            var model = {};

            model.code = CodeEditor.editor.getValue();
            model.name = $('#model-name').val();

            if (modelId === '') {
                FredBar.createModel(model);
            } else {
                FredBar.modifyModel(modelId, model);
            }
        }

        return FredBar;
    }
);