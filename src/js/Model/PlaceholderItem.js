/**
 *  A module representing PlaceholderItem.
 *  @namespace Model/Body
 */
define(
    'Model/PlaceholderItem',
    [],
    function PlaceholderItemFactory () {
        "use strict";

        return function PlaceholderItem (settings) {
            this.type = settings.type;
        };

    }
);