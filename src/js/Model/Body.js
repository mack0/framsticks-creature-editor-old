/**
 *  A module representing Body.
 *  @namespace Model
 */
define(
    'Model/Body',
    [
        'Configuration/configuration-module',

        'Model/Body/Comment',
        'Model/Body/Connection',
        'Model/Body/Joint',
        'Model/Body/ModelProperties',
        'Model/Body/Neuron',
        'Model/Body/Part',
        'Model/PlaceholderItem'
    ],
    function BodyModule (config, Comment, Connection, Joint, ModelProperties, Neuron, Part, PlaceholderItem) {
        "use strict";

        /////////////////// API
        /**
         * @class
         * @memberof ModelBody
         */
        var Body = {
            // getters and setters
            get objects         ()      { return objects;       },
            get events          ()      { return events;        },
            get placeholders    ()      { return placeholders;  },
            set objects         (value) { objects = value;      },
            set events          (value) { events = value;       },
            set placeholders    (value) { placeholders = value; },

            // regular methods
            addComment: addComment,
            addConnection: addConnection,
            addJoint: addJoint,
            addModelProperties: addModelProperties,
            addNeuron: addNeuron,
            addObject: addObject,
            addPart: addPart,
            addPlaceholderItem: addPlaceholderItem,

            generateCodeFromModel: generateCodeFromModel,

            get3dObjectByLine: get3dObjectByLine,
            getJointsByPartRefNumber: getJointsByPartRefNumber,
            getJointsByPartRefNumberWithDistinction: getJointsByPartRefNumberWithDistinction,
            getNextPredictedData: getNextPredictedData,
            getObjectByLine: getObjectByLine,
            getObjectByRefNumber: getObjectByRefNumber,

            manageObjects: manageObjects,
            modifyObject: modifyObject,

            off: off,
            on: on,

            removeAllObjects: removeAllObjects,
            removeObjectByLine: removeObjectByLine,
            removeObjectFromSceneByLine: removeObjectFromSceneByLine,

            triggerCallbacks: triggerCallbacks,
            triggerEvent: triggerEvent,

            updateLinesAfterRemove: updateLinesAfterRemove
        };

        /////////////////// Data store
        var objects = [],
            events = {},
            placeholders = [];

        /////////////////// Methods
        /**
         * Adds new Comment to objects.
         *
         * @memberof ModelBody
         * @param {Object} settings - Settings for new Comment.
         */
        function addComment (settings) {
            Body.objects.push(new Comment(settings));
        }

        /**
         * Adds new Connection to objects.
         *
         * @memberof ModelBody
         * @param {Object} settings - Settings for new Connection.
         */
        function addConnection (settings) {
            Body.objects.push(new Connection(settings));
        }

        /**
         * Adds new Joint to objects.
         *
         * @memberof ModelBody
         * @param {Object} settings - Settings for new Joint.
         */
        function addJoint (settings) {
            Body.objects.push(new Joint(settings));
        }

        /**
         * Adds new Model Properties to objects.
         *
         * @memberof ModelBody
         * @param {Object} settings - Settings for new Model Properties.
         */
        function addModelProperties (settings) {
            Body.objects.push(new ModelProperties(settings));
        }

        /**
         * Adds new Neuron to objects.
         *
         * @memberof ModelBody
         * @param {Object} settings - Settings for new Neuron.
         */
        function addNeuron (settings) {
            Body.objects.push(new Neuron(settings));
        }

        /**
         * Adds new Object
         *
         * @memberof ModelBody
         * @param {Object} settings - Settings for new Object.
         */
        function addObject (settings) {
            switch (settings.type) {
                case '#':   Body.addComment(settings);          break;
                case 'c':   Body.addConnection(settings);       break;
                case 'j':   Body.addJoint(settings);            break;
                case 'm':   Body.addModelProperties(settings);  break;
                case 'n':   Body.addNeuron(settings);           break;
                case 'p':   Body.addPart(settings);             break;
                default :                                       break;
            }
        }

        /**
         * Adds new Part to objects.
         *
         * @memberof ModelBody
         * @param {Object} settings - Settings for new Part.
         */
        function addPart (settings) {
            Body.objects.push(new Part(settings));
        }

        /**
         * Adds new Placeholder Item to objects.
         *
         * @memberof ModelBody
         * @param {Object} settings - Settings for new Placeholder item.
         */
        function addPlaceholderItem (settings) {
            Body.objects.push(new Part(settings));
        }

        /**
         * Generates f0 genotype code from model.
         *
         * @memberof ModelBody
         * @returns {String} - f0 genotype code.
         */
        function generateCodeFromModel () {
            var code = '';

            for (var i = 0; i < Body.objects.length; i++) {
                code += Body.objects[i].generateCode();
                if (i < Body.objects.length - 1) {
                    code += config.constants.LINE_SEPARATOR;
                }
            }

            return code;
        }

        /**
         * Returns 3d representation of object by given line.
         *
         * @memberof ModelBody
         * @param {Number} line - Line in f0 genotype code editor.
         * @returns {Object|Null}
         */
        function get3dObjectByLine (line) {
            var object = Body.getObjectByLine(line);

            return (object !== null) ? object.model3d : null;
        }

        function getJointsByPartRefNumber(refNumber) {
            var object = Body.getObjectByRefNumber(refNumber, 'p'),
                joints = [];

            if (object !== null) {
                for (var i = 0, len = objects.length; i < len; i++) {
                    if (objects[i].model !== undefined && objects[i].type === 'j') {
                        if (objects[i].model.p1.value === object.refNumber || objects[i].model.p2.value === object.refNumber) {
                            joints.push(objects[i]);
                        }
                    }
                }
            }

            return joints;
        }

        function getJointsByPartRefNumberWithDistinction(refNumber) {
            var joints = Body.getJointsByPartRefNumber(refNumber);
            var partAsP1 = [];
            var partAsP2 = [];

            for (var i = 0, len = joints.length; i < len; i++) {
                if (joints[i].model.p1.value === refNumber) {
                    partAsP1.push(joints[i]);
                }

                if (joints[i].model.p2.value === refNumber) {
                    partAsP2.push(joints[i]);
                }
            }

            return {
                partAsP1: partAsP1,
                partAsP2: partAsP2
            };
        }

        /**
         * Returns predicted next line and next ref number.
         *
         * @param {String} type
         * @returns {Object}
         */
        function getNextPredictedData (type) {
            var lastLine = 0, lastRefNumber = 0;

            for (var i = 0, len = objects.length; i < len; i++) {
                if (lastLine < objects[i].line) {
                    lastLine = objects[i].line
                }
                if (lastRefNumber < objects[i].refNumber && objects[i].type === type) {
                    lastRefNumber = objects[i].refNumber
                }
            }

            return {
                nextLine: ++lastLine,
                nextRefNumber: ++lastRefNumber
            }
        }

        /**
         * Returns object by given line.
         *
         * @memberof ModelBody
         * @param {Number} line - Line in f0 genotype code editor.
         * @returns {Object|Null}
         */
        function getObjectByLine (line) {

            for (var i = 0; i < Body.objects.length; i++) {
                if (Body.objects[i].line === line) {
                    return Body.objects[i];
                }
            }

            return null;
        }

        /**
         * Returns Object by given reference number.
         *
         * Parts, Joint, Neurons, Connections and Model Properties have reference numbers used to attach other objects.
         * References start with 0 and every new object in the class gets the next reference number.
         *
         * @memberof ModelBody
         * @param {Number} refNumber - Reference number.
         * @param {String} type - Type of object
         * @returns {Object|Null}
         */
        function getObjectByRefNumber (refNumber, type) {
            for (var i = 0, len = objects.length; i < len; i++) {
                if (objects[i].refNumber === refNumber && objects[i].type === type) {
                    return objects[i];
                }
            }

            return null;
        }

        /**
         * Manages objects by adding new ones or editing object that were
         * already created.
         *
         * @memberof ModelBody
         * @param {Object} settings - Settings for Object.
         */
        function manageObjects (settings) {
            var object = Body.getObjectByLine(settings.line);

            if (typeof settings.type !== 'undefined') {
                if (object === null) {
                    Body.addObject(settings);
                } else {
                    if (settings.type !== object.type) {
                        /**
                         * Object in this line has different type that object in memory.
                         * Old object needs to be removed and new must be rendered.
                         * Simple modifying object would not suffice.
                         */
                        replaceObjectByLine(object, settings);
                        //removeObjectByLine(settings.line);
                        //Body.addObject(settings);
                    } else {
                        Body.modifyObject(settings);
                    }
                }
            }
        }

        function replaceObjectByLine (object, settings) {
            var index = objects.indexOf(object);

            if (index > -1) {
                if (object.model3d) {
                    Body.removeObjectFromSceneByLine(object.line, object.model3d.parent);
                }

                switch (settings.type) {
                    case '#':   objects[index] = new Comment(settings);         break;
                    case 'c':   objects[index] = new Connection(settings);      break;
                    case 'j':   objects[index] = new Joint(settings);           break;
                    case 'm':   objects[index] = new ModelProperties(settings); break;
                    case 'n':   objects[index] = new Neuron(settings);          break;
                    case 'p':   objects[index] = new Part(settings);            break;
                    default :                                                   break;
                }

                updateRefNumbers();
            }
        }

        /**
         * Adds new Object
         *
         * @memberof ModelBody
         * @param {Object} settings - Settings for new Object.
         */
        function modifyObject (settings) {
            var object = Body.getObjectByLine(settings.line),
                t = settings.type;

            if (t === '#' || t === 'c' || t === 'm' || t === 'n' || t === 'p' || t === 'j') {
                object.modify(settings);
            }
        }

        /**
         * Detaches event and callback.
         *
         * @memberof ModelBody
         * @param {String} event - Name of event.
         * @param {Function} callback - Origin event callback.
         */
        function off (event, callback) {
            var index = -1;

            if (Array.isArray(Body.events[event])) {
                index = Body.events[event].indexOf(callback);
            }

            if (index > -1) {
                Body.events[event].splice(index, 1);
            }
        }

        /**
         * Attaches event and callback.
         *
         * @memberof ModelBody
         * @param {String} event - Name of event.
         * @param {Function} callback - Origin event callback.
         */
        function on (event, callback) {
            if (!Array.isArray(Body.events[event])) {
                Body.events[event] = [];
            }

            Body.events[event].push(callback);
        }

        /**
         * Removes all objects and 3d representation from scene.
         *
         * @memberof ModelBody
         */
        function removeAllObjects () {
            var i;

            for (i = objects.length - 1; i >= 0; i-- ) {
                Body.removeObjectByLine(objects[i].line);
            }

            objects = [];
        }

        /**
         * Removes object and 3d representation from scene by given code line.
         *
         * @memberof ModelBody
         * @param {Number} line - Line in f0 genotype code editor.
         * @param {boolean} [updateLines] - Indicates if lines counts should be updated.
         */
        function removeObjectByLine (line, updateLines) {
            var object = Body.getObjectByLine(line),
                objects = Body.objects,
                index = objects.indexOf(object);

            if (index > -1) {
                if (object.type === 'j' || object.type === 'p') {
                    if (object.model3d !== null) {
                        Body.removeObjectFromSceneByLine(line, object.model3d.parent);
                    }
                }
                objects.splice(index, 1);

                if (updateLines) {
                    Body.updateLinesAfterRemove(index);
                }
            }
        }

        /**
         * Removes 3d representation from scene by given code line.
         *
         * @memberof ModelBody
         * @param {Number} line - Line in f0 genotype code editor.
         * @param {Object} scene - Scene.
         */
        function removeObjectFromSceneByLine (line, scene) {
            var object = Body.getObjectByLine(line);

            if (object !== null) {
                if (typeof object.localAxes !== 'undefined') {
                    object.model3d.remove(object.localAxes);
                    object.localAxes = undefined;
                }
                scene.remove(object.model3d);
                object.model3d = null;
            }
        }

        /**
         * Triggers all callbacks subscribed to event except the event that come from origin event trigger.
         *
         * @memberof ModelBody
         * @param {Array} callbacks - Array with callbacks subscribed to event.
         * @param {Function} callback - Origin event callback.
         */
        function triggerCallbacks (callbacks, callback) {
            for (var i = 0; i < callbacks.length; i++) {
                if (typeof callbacks[i] === 'function' && callbacks[i] !== callback) {
                    callbacks[i]();
                }
            }
        }

        /**
         * Triggers subscribed event.
         *
         * @memberof ModelBody
         * @param {String} event - Name of event.
         * @param {Function} callback - Origin event callback.
         */
        function triggerEvent (event, callback) {
            var callbacks = Body.events[event];

            if (Array.isArray(callbacks)) {
                Body.triggerCallbacks(callbacks, callback);
            }
        }

        /**
         * Updates lines after removed item.
         *
         * @param {Number} removedIndex - Index in array of objects of removed item.
         */
        function updateLinesAfterRemove(removedIndex) {
            for (var i = removedIndex, len = objects.length; i < len; i++) {
                objects[i].line--;

                if (objects[i].model3d) {
                    objects[i].model3d.line--;
                }
            }
        }

        function updateRefNumbers () {
            var refNumberComment        = 0;
            var refNumberConnection     = 0;
            var refNumberJoint          = 0;
            var refNumberModelProperties= 0;
            var refNumberNeuron         = 0;
            var refNumberPart           = 0;

            for (var i = 0, len = objects.length; i < len; i++) {
                switch (objects[i].type) {
                    case '#':   objects[i].refNumber = refNumberComment++;          break;
                    case 'c':   objects[i].refNumber = refNumberConnection++;       break;
                    case 'm':   objects[i].refNumber = refNumberModelProperties++;  break;
                    case 'n':   objects[i].refNumber = refNumberNeuron++;           break;
                    case 'p':   objects[i].refNumber = refNumberPart++;             break;
                    case 'j':   objects[i].refNumber = refNumberJoint++;            break;
                    default :                                                       break;
                }
            }
        }

        window.debug = {
            objects: objects,
            Body: Body
        };

        return Body;
    }
);