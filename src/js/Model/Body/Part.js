/**
 *  A module representing Part.
 *  @namespace Model/Body
 */
define(
    'Model/Body/Part',
    ['jquery', 'Model/Body/Item', 'xmlModel/xmlModelService'],
    function PartFactory($, Item, xmlModel) {
        "use strict";

        return function Part(settings) {
            Item.call(this, settings);

            this.type = 'p';

            this.line = settings.line;
            this.refNumber = settings.refNumber;
            this.model3d = null;
            this.model = {};

            // DEFAULT OPTIONS
            this.defaults = $.extend(true, {}, xmlModel.classProps.p.defaults);
            this.propSequence = xmlModel.classProps.p.propSequence.slice();

            /////////////////// Override Methods
            /**
             * Returns html for hover tooltip with very basic information.
             *
             * @memberof Part
             * @returns {string}
             */
            this.generateHoverTooltip = function () {
                var code = '';

                code += '<p><i class="fa fa-globe"></i> ' + this.refNumber + '</p>';
                code += '<p><i class="fa fa-bookmark"></i> Part</p>';

                return code;
            };

            /////////////////// Initialization
            this.parse(settings.code);
        };

    }
);