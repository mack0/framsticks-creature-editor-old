/**
 *  A module representing Joint.
 *  @namespace Model/Body
 */
define(
    'Model/Body/Joint',
    ['jquery', 'Model/Body/Item', 'xmlModel/xmlModelService'],
    function JointFactory($, Item, xmlModel) {
        "use strict";

        return function Joint(settings) {
            Item.call(this, settings);

            this.type = 'j';

            this.line = settings.line;
            this.model3d = null;
            this.model = {};

            // DEFAULT OPTIONS
            this.defaults = $.extend(true, {}, xmlModel.classProps.j.defaults);
            this.propSequence = xmlModel.classProps.j.propSequence.slice();

            /////////////////// Override Methods
            /**
             * Returns html for hover tooltip with very basic information.
             *
             * @memberof Joint
             * @returns {string}
             */
            this.generateHoverTooltip = function () {
                var code = '';

                code += '<p><i class="fa fa-globe"></i> ' + this.refNumber + '</p>';
                code += '<p><i class="fa fa-bookmark"></i> Joint</p>';
                code += '<p>' + this.model.p1.value + ' <i class="fa fa-link"></i> ' + this.model.p2.value + '</p>';

                return code;
            };

            /**
             * Modifies Joint state.
             *
             * @memberof Joint
             * @param {Object} settings
             */
            this.modify = function (settings) {
                this.parse(settings.code);

                this.line = settings.line;
                if (this.model3d !== null) {
                    this.model3d.p1 = this.model.p1.value;
                    this.model3d.p1 = this.model.p2.value;
                }
            };

            /////////////////// Initialization
            this.parse(settings.code);
        };

    }
);