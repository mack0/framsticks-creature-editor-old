/**
 *  A module representing Item.
 *  @namespace Model/Body
 */
define(
    'Model/Body/Item',
    ['jquery'],
    function ItemFactoryModule($) {
        "use strict";

        function Item(settings) {
            var self = this,
                modelAnnotations = [];

            this.type = 'item';

            this.line = settings.line;
            this.refNumber = settings.refNumber;
            this.model3d = null;
            this.model = {};

            // DEFAULT OPTIONS
            this.defaults = {};
            this.propSequence = [];

            /////////////////// Methods
            /**
             * Adds 3d representation of model.
             *
             * @memberof Item
             * @param {Object} model3d - Model3d.
             */
            this.add3dRepresentation = function (model3d) {
                this.model3d = model3d;
            };

            /**
             * Returns f0 code from current model state.
             *
             * @memberof Item
             * @returns {String}
             */
            this.generateCode = function () {
                var code, prop;

                code = this.type + ': ';

                for (var i = 0, len = this.propSequence.length; i < len; i++) {
                    prop = this.model[this.propSequence[i]];

                    if (typeof prop !== 'undefined' && !prop.ommit) {
                        if (!prop.shortNotation) {
                            code += this.propSequence[i] + '=';
                        }

                        code += prop.value + ', ';
                    }
                }

                if (code[code.length - 2] === ',' && code[code.length - 1] === ' ') {
                    code = code.substring(0, code.length - 2);
                }

                return code;
            };

            /**
             * Returns html for Context Menu for object.
             * It provides ability for editing object in 3D scene by editing proper input with property value.
             *
             * @memberof Item
             * @returns {string}
             */
            this.generateContextMenu = function () {
                var html = '', prop, nextProp, propTooltip, nextPropTooltip,
                    half = Math.ceil(this.propSequence.length / 2);

                html += '<form><table>';

                for (var i = 0; i < half; i++) {
                    prop = this.model[this.propSequence[i]];
                    nextProp = this.model[this.propSequence[i+half]];

                    propTooltip = self.generateContextMenuTooltip(this.model[this.propSequence[i]]);

                    html += '<tr>';

                    html += '<td class="propName" title="'+propTooltip+'">' + (prop.name ? prop.name : this.propSequence[i]) + '</td>';
                    html += '<td title="'+propTooltip+'">';
                        html += '<input type="text" ';
                        html += 'tabindex="' + (i+1) + '" ';
                        html += 'data-model-line="' + this.line + '" ';
                        html += 'data-prop-name="' + this.propSequence[i] + '" ';
                        html += 'value="' + prop.value + '">';
                    html += '</td>';

                    if (typeof nextProp !== 'undefined') {
                        nextPropTooltip = self.generateContextMenuTooltip(this.model[this.propSequence[i+half]]);

                        html += '<td class="propName" title="'+nextPropTooltip+'">' + (nextProp.name ? nextProp.name : this.propSequence[i+half]) + '</td>';
                        html += '<td title="'+nextPropTooltip+'">';
                            html += '<input type="text" ';
                            html += 'tabindex="' + (i+half+1) + '" ';
                            html += 'data-model-line="' + this.line + '" ';
                            html += 'data-prop-name="' + this.propSequence[i+half] + '" ';
                            html += 'value="' + nextProp.value + '">';
                        html += '</td>';
                    }

                    html += '</tr>';
                }

                html += '</table></form>';

                return html
            };

            this.generateContextMenuTooltip = function (property) {
                var tooltip = property.name + ': ',
                    tooltipProps = ['id', 'default', 'min', 'max'];

                for (var i = 0, len = tooltipProps.length; i < len; i++) {
                    if (typeof property[tooltipProps[i]] !== 'undefined') {
                        tooltip += tooltipProps[i] + ': ' + property[tooltipProps[i]] + ', ';
                    }
                }

                return tooltip.substring(0, tooltip.length - 2);
            };

            /**
             * Modifies Item state.
             *
             * @memberof Item
             * @param {Object} settings
             */
            this.modify = function (settings) {
                this.line = settings.line;
                this.parse(settings.code);
            };

            /**
             * Parses code into model.
             *
             * @memberof Item
             * @param {String} code - f0 code.
             */
            this.parse = function (code) {
                var propertiesString, properties, prop,
                    descriptor, value,
                    shortNotation,
                    previousDescriptor = '',
                    splitByCommasReg = /,(?=(?:(?:[^"]*"){2})*[^"]*$)/,
                    splitByEqualSignReg = /=(?=(?:(?:[^"]*"){2})*[^"]*$)/,
                    warningAnnotation, warnings = [], errorAnnotation, errors = [];

                modelAnnotations = [];

                this.model = $.extend(true, {}, this.defaults);

                propertiesString = code.substring(2, code.length);
                properties = propertiesString.split(splitByCommasReg);

                if (properties[0]) {
                    for (var i = 0, len = properties.length; i < len; i++) {
                        prop = properties[i].split(splitByEqualSignReg);

                        if (prop.length > 1) {
                            // syntax with descriptor used, e.g. rx=3
                            descriptor = prop[0];
                            if (this.propSequence.indexOf(descriptor) > -1) {
                                // set only when descriptor is present in properties sequence
                                value = prop[1] || this.defaults[descriptor].value;
                                shortNotation = false;
                            }

                            previousDescriptor = descriptor;
                        } else {
                            // used shorter syntax without descriptor, e.g. 3, descriptor is calculated from sequence

                            if (previousDescriptor === 'x' || previousDescriptor === 'y' || previousDescriptor === 'z') {
                                descriptor = this.propSequence[this.propSequence.indexOf(previousDescriptor) + 1];
                            }
                            else if (previousDescriptor.length === 2) {
                                // indicator of group used!
                                // when user adds rx=1,2,3 in any place, it should be treated as rx=1,ry=2,rz=3
                                descriptor = this.propSequence[this.propSequence.indexOf(previousDescriptor) + 1];
                            } else {
                                // descriptor defined by sequence
                                descriptor = this.propSequence[this.propSequence.indexOf(previousDescriptor) + 1];
                                if (this.propSequence[i] !== descriptor) {
                                    warnings.push('Missing property name in "p" (assuming "' + descriptor + '");');
                                }
                            }

                            previousDescriptor = descriptor;

                            if ((typeof this.defaults[descriptor].def !== 'undefined' && this.defaults[descriptor].def !== this.model[descriptor].value)
                                || (typeof this.defaults[descriptor].def === 'undefined' && this.model[descriptor].value !== 0 && (descriptor === 'x' || descriptor === 'y' || descriptor === 'z'))) {
                                value = this.model[descriptor].value;
                            } else {
                                if (prop[0] === '' || prop[0].charCodeAt(0) === 13) {
                                    value = this.defaults[descriptor].def;
                                } else {
                                    value = prop[0];
                                }
                            }
                            shortNotation = true;
                        }

                        if (typeof this.model[descriptor] !== 'undefined') {
                            if (this.propSequence.indexOf(descriptor) > -1) {
                                this.setModelPropValue(this.model[descriptor], value);
                                this.model[descriptor].ommit = false;
                                this.model[descriptor].shortNotation = shortNotation;

                                warningAnnotation = this.setWarningAnnotation(descriptor, value);
                                errorAnnotation = this.setErrorAnnotation(descriptor, value);
                                if (warningAnnotation !== '') {
                                    warnings.push(warningAnnotation);
                                }
                                if (errorAnnotation !== '') {
                                    errors.push(errorAnnotation);
                                }
                            }
                        } else {
                            warnings.push(descriptor + ' is not a valid property;');
                        }
                    }
                }

                this.setAnnotations(warnings, errors);
            };

            this.setErrorAnnotation = function (descriptor, value) {
                var text = '';

                if (this.model[descriptor].type === 'f' && value[0] !== '"' && value[value.length-1] !== '"' && isNaN(value)) {
                    text += descriptor + ' accepts float type, does not accept value "' + value + '"; ';
                }

                if (this.model[descriptor].type === 'd' && value[0] !== '"' && value[value.length-1] !== '"' && (isNaN(value) || parseInt(value) !== parseFloat(value))) {
                    text += descriptor + ' accepts integer type, does not accept value "' + value + '"; ';
                }
                if (this.type === 'j' && (descriptor === 'p1' || descriptor === 'p2') && value === -1) {
                    text += descriptor + ' accepts integer type, does not accept empty value; ';
                }

                return text;
            };

            this.setWarningAnnotation = function (descriptor, value) {
                var text = '';

                if (this.model[descriptor].type === 'f' && typeof this.model[descriptor].min !== 'undefined' && value < this.model[descriptor].min) {
                    text += descriptor + ' cannot be less than ' + this.model[descriptor].min + '; ';
                }
                if (this.model[descriptor].type === 'f' && typeof this.model[descriptor].max !== 'undefined' && value > this.model[descriptor].max) {
                    text += descriptor + ' cannot be greater than ' + this.model[descriptor].max + '; ';
                }
                return text;
            };

            this.setAnnotations = function (warnings, errors) {
                var warningsText = '', errorsText = '',
                    warningsAnnotations, errorsAnnotations;

                if (warnings.length > 0) {
                    warningsText = warnings.reduce(function (prev, current) {
                        return prev + current;
                    });

                    if (warningsText !== '') {
                        warningsAnnotations = {
                            row: this.line,
                            column: 1,
                            text: warningsText,
                            type: 'warning'
                        };
                        modelAnnotations.push(warningsAnnotations);
                    }
                }
                if (errors.length > 0) {
                    errorsText = errors.reduce(function (prev, current) {
                        return prev + current;
                    });

                    if (errorsText !== '') {
                        errorsAnnotations = {
                            row: this.line,
                            column: 1,
                            text: errorsText,
                            type: 'error'
                        };
                        modelAnnotations.push(errorsAnnotations);
                    }
                }
            };

            this.getModelAnnotations = function () {
                return modelAnnotations;
            };

            /**
             * Sets value for property in model. Value is given as a string.
             * Method converts this string to proper type if needed or leave it as it is.
             *
             * @memberof Item
             * @param {Object} prop - Model's property.
             * @param {String} value - Value to set for property.
             */
            this.setModelPropValue = function (prop, value) {
                if (prop.type === 'f' && value[0] === '"' && value[value.length-1] === '"') {
                    value = value.substring(1,value.length-1);
                }

                if (prop.type === 'f') {
                    prop.value = parseFloat(value);
                } else if (prop.type === 'd') {
                    prop.value = parseInt(value);
                } else {
                    prop.value = value;
                }
            }
        }

        return Item;
    }
);