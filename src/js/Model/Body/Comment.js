/**
 *  A module representing Comment.
 *  @namespace Model/Body
 */
define(
    'Model/Body/Comment',
    [],
    function CommentFactory () {
        "use strict";

        return function Comment (settings) {
            this.type = '#';

            this.line = settings.line;
            this.refNumber = settings.refNumber;
            this.value = settings.value;

            this.generateCode = function () {
                return this.value;
            };

            this.modify = function (settings) {
                this.line = settings.line;
                this.value = settings.value;
            };
        };

    }
);