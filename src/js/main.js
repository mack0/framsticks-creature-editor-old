function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
}
function isTheSameOrigin() {
    try {
        return window.frameElement !== null;
    } catch (e) {
        return false;
    }
}
function isCameraOrTargetSet() {
    return window.FREDJS_EMBED_CAMERA_X !== null || window.FREDJS_EMBED_CAMERA_Y !== null || window.FREDJS_EMBED_CAMERA_Z !== null ||
    window.FREDJS_EMBED_TARGET_X !== null || window.FREDJS_EMBED_TARGET_Y !== null || window.FREDJS_EMBED_TARGET_Z !== null;
}
var embedSettings = ['autorotate', 'show-grid', 'show-axes', 'show-local-axes', 'show-wireframe', 'camera-x', 'camera-y', 'camera-z', 'target-x', 'target-y', 'target-z', 'genotype', 'db-url'];
function setEmbedSettings () {
    var transformed, i, len;

    if (isTheSameOrigin()) {
        for (i = 0, len = embedSettings.length; i < len; i++) {
            transformed = embedSettings[i].replace(/-/g, '_').toUpperCase();
            window['FREDJS_EMBED_' + transformed] = window.frameElement.getAttribute(embedSettings[i]);
        }
    } else {
        for (i = 0, len = embedSettings.length; i < len; i++) {
            transformed = embedSettings[i].replace(/-/g, '_').toUpperCase();
            window['FREDJS_EMBED_' + transformed] = getURLParameter(embedSettings[i]);
        }
    }

    if (window.FREDJS_EMBED_DB_URL !== null) {
        window.FREDJS_EMBED_DB_URL = decodeURIComponent(window.FREDJS_EMBED_DB_URL);
    }
}

setEmbedSettings();

requirejs.config({
    paths: {
        ace: '../libs/ace-builds/src-min',
        jquery: '../vendors/jquery',
        jqueryui: '../libs/jquery-ui-1.11.4.custom/jquery-ui',
        OrbitControls: '../libs/threejs-extensions/OrbitControls',
        THREE: '../vendors/three',
        text: '../vendors/text',
        Q: '../vendors/q'
    },
    shim: {
        'OrbitControls': {
            deps: ['THREE']
        },
        'THREE': {
            exports: 'THREE'
        }
    }
});

requirejs(
    [
        'jquery',
        'CodeEditor/code-editor-module',
        'Configuration/configuration-module',
        'text!../model/f0def.xml',
        'FredBar/fred-bar-module',
        'xmlModel/xmlModelService',

        'jqueryui'
    ],
    function appInit ($, CodeEditor, config, genotypeXMLFile, FredBar, xmlModelService) {
        "use strict";

        xmlModelService.initializeModelsFromXML(genotypeXMLFile).then(function () {
            FredBar.init();
            CodeEditor.init();

            $('#global-spinner').hide();
        });
    });
