/**
 *  A module representing Utils.
 *  @namespace Utils
 */
define(
    'Utils/utils-module',
    [],
    function UtilsModule () {
        "use strict";

        /////////////////// API
        /**
         * @class
         * @memberof Utils
         */
        var Utils = {
            convertAttrValueToBool: convertAttrValueToBool,
            getDataAttribute: getDataAttribute,
            hasDuplicates: hasDuplicates,
            hexToNormalizedRGB: hexToNormalizedRGB,
            rotateAboutWorldAxes: rotateAboutWorldAxes,
            rotateAboutWorldAxis: rotateAboutWorldAxis
        };

        /////////////////// Methods

        /**
         * Return true if given value is "true", false if given value is "false" and otherwise it returns undefined
         * @param value
         * @returns {boolean}
         */
        function convertAttrValueToBool (value) {
            if (value === 'true') {
                return true;
            } if (value === 'false') {
                return false;
            }
        }

        /**
         * Return data from element by attribute name.
         *
         * @param {DOMElement} el
         * @param {String} attributeName
         * @returns {*}
         */
        function getDataAttribute (el, attributeName) {
            if (el.dataset) {
                return el.dataset[attributeName];
            } else {
                return el.getAttribute(attributeName);
            }
        }

        function hasDuplicates(array) {
            var valuesSoFar = Object.create(null);
            for (var i = 0; i < array.length; ++i) {
                var value = array[i];
                if (value in valuesSoFar) {
                    return true;
                }
                valuesSoFar[value] = true;
            }
            return false;
        }

        /**
         * Converts HEX to normalized RGB.
         *
         * @memberof Utils
         * @param {String} hex - HEX representation of color.
         * @return {Object|null}
         */
        function hexToNormalizedRGB (hex) {
            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

            return result ? {
                r: parseInt(result[1], 16) / 255,
                g: parseInt(result[2], 16) / 255,
                b: parseInt(result[3], 16) / 255
            } : null;
        }

        /**
         * Rotates given object around axes by given angles.
         *
         * @param {Object} object - ThreeJS Object
         * @param {Object} axesAngles
         * @param {Number} [axesAngles.x]
         * @param {Number} [axesAngles.y]
         * @param {Number} [axesAngles.z]
         */
        function rotateAboutWorldAxes(object, axesAngles) {
            var xAxis = new THREE.Vector3(1,0,0),
                yAxis = new THREE.Vector3(0,1,0),
                zAxis = new THREE.Vector3(0,0,1);

            if (typeof axesAngles.z !== 'undefined') {
                Utils.rotateAboutWorldAxis(object, zAxis, axesAngles.z);
            }
            if (typeof axesAngles.y !== 'undefined') {
                Utils.rotateAboutWorldAxis(object, yAxis, -axesAngles.y);
            }
            if (typeof axesAngles.x !== 'undefined') {
                Utils.rotateAboutWorldAxis(object, xAxis, axesAngles.x);
            }
        }

        /**
         * Rotates given object around given axis by given angle
         * @param object
         * @param axis
         * @param angle
         */
        function rotateAboutWorldAxis(object, axis, angle) {
            var rotationMatrix = new THREE.Matrix4();
            rotationMatrix.makeRotationAxis( axis.normalize(), angle );
            var currentPos = new THREE.Vector4(object.position.x, object.position.y, object.position.z, 1);
            var newPos = currentPos.applyMatrix4(rotationMatrix);
            object.position.x = newPos.x;
            object.position.y = newPos.y;
            object.position.z = newPos.z;
        }

        return Utils;
    }
);