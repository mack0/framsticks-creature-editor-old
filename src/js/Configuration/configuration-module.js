/**
 *  A module representing Configuration.
 *  @namespace Configuration
 */
define(
    'Configuration/configuration-module',
    [],
    function ConfigurationModule () {
        "use strict";

        var constants, values;

        // Constant values
        constants = {
            CURSOR_ADD: 'cell',
            CURSOR_AUTO: 'auto',
            CURSOR_MOVE: 'move',
            CURSOR_POINTER: 'pointer',
            HALF_PI: Math.PI * 0.5,
            LINE_SEPARATOR: '\n'
        };

        // Configuration values
        values = {
            wireframe: false,

            // Camera default position
            camera: {
                position: {
                    x: -3,
                    y: 8,
                    z: 7.2
                }
            },

            // Colors:
            hoverColor: '#00ff00',
            jointColor: '#424346',
            partColor: '#0b336b',
            rendererBg: '#191919',

            // Helpers:
            autorotate: false,
            axes: true,
            grid: true,
            localAxes: false,

            embedUseDomain: 'embedEditorUrlSameDomain',
            embedEditorUrlSameDomain: '/files/apps/js/creature-editor/',
            embedEditorUrlCDN: 'http://www.framsticks.com/files/apps/js/creature-editor/',
            embedDefaults: {
                "autorotate": 'false',
                "show-axes": 'false',
                "show-grid": 'true',
                "wireframe": 'false',
                "camera-x": -3,
                "camera-y": 8,
                "camera-z": 7.2,
                "target-x": 0,
                "target-y": 0,
                "target-z": 0
            },

            // f0 specifics not listed in genotype description
            joint: {
                invisible: {
                    opacity: 0.2,
                    radius: 0.05
                },
                radius: 0.2
            },
            part: {
                radius: 0.2
            }
        };

        return {
            mode: {
                embed: !!window.FREDJS_EMBED
            },
            constants: constants,
            values: values
        };
    }
);