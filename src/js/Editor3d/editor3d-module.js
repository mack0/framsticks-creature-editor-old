/**
 *  A module representing Editor3d.
 *  @namespace Editor3d
 */
define(
    'Editor3d/editor3d-module',
    [
        'jquery',
        'THREE',
        'Configuration/configuration-module',
        'Utils/utils-module',
        'Editor3d/Scene/Axis/axis-module',
        'Editor3d/Scene/Box/box-module',
        'Editor3d/Scene/Camera/camera-module',
        'Editor3d/Scene/Controls/controls-module',
        'Editor3d/Scene/Cylinder/cylinder-module',
        'Editor3d/Scene/Lights/lights-module',
        'Editor3d/Scene/ModelGroup/model-group-module',
        'Editor3d/Scene/Sphere/sphere-module',

        'Model/Body',

        'OrbitControls'
    ],
    function ($, THREE, config, Utils, Axis, Box, Camera, Controls, Cylinder, Lights, ModelGroup, Sphere, Body) {
        "use strict";

        /////////////////// API
        /**
         * @class
         * @memberof Editor3d
         */
        var Editor3d = {
            get scene () { return scene; }
        };

        /////////////////// Data store
        var axes, grid, scene, camera, controls, renderer, raycaster, planeMoveHelper;

        var editor3dEl, editor3dContentEl, editor3dContainerEl, $editorContextMenuEl, $hoverTooltipEl, editorWidth, editorHeight;

        var editorControlsEl, $embedSettingsEl;

        var model, placeholderItem;

        var mode;

        var keyControls = {
            c: false,
            x: false,
            z: false
        };

        var jointProcess = {
                p1: null,
                p2: null
            };

        var mouse = new THREE.Vector2(),
            offset = new THREE.Vector3(),
            INTERSECTED, SELECTED;

        /////////////////// Initialization Phase
        _init();

        /////////////////// Private Methods

        function _addJoint (INTERSECTED) {
            var predictedData, settings;

            if (mode === 'addingJoint1' || mode === 'addingJoint2') {
                INTERSECTED.material.color.setStyle(config.values.hoverColor);

                if (jointProcess.p1 === null && jointProcess.p2 === null) {
                    jointProcess.p1 = Body.getObjectByLine(INTERSECTED.line);

                    mode = 'addingJoint2';
                } else if (jointProcess.p1 !== null && jointProcess.p2 === null && jointProcess.p1.line !== INTERSECTED.line) {
                    jointProcess.p2 = Body.getObjectByLine(INTERSECTED.line);

                    predictedData = Body.getNextPredictedData('j');
                    settings = {
                        line: predictedData.nextLine,
                        refNumber: predictedData.nextRefNumber,
                        type: 'j'
                    };
                    settings.code = 'j: ' + jointProcess.p1.refNumber + ', ' + jointProcess.p2.refNumber;

                    jointProcess.p1.model3d.material.color.setRGB(jointProcess.p1.model.vr.value, jointProcess.p1.model.vg.value, jointProcess.p1.model.vb.value);
                    jointProcess.p2.model3d.material.color.setRGB(jointProcess.p2.model.vr.value, jointProcess.p2.model.vg.value, jointProcess.p2.model.vb.value);

                    if (Body.objects[Body.objects.length - 1].type === '#' && Body.objects[Body.objects.length - 1].value === '' && Body.objects[Body.objects.length - 1].refNumber !== 0) {
                        Body.removeObjectByLine(Body.objects[Body.objects.length - 1].line);
                        settings.line--;
                    }

                    Body.addJoint(settings);
                    Body.triggerEvent('change', _onBodyChange);

                    mode = 'idle';

                    jointProcess = {
                        p1: null,
                        p2: null
                    };

                    _centerView();
                }
            }
        }

        /**
         * Begins process of adding Joint to the scene.
         *
         * @private
         * @memberof Editor3d
         * @param {Object} ev - Onclick event.
         * @param {Object} control - Control object with information about placeholder item.
         * @param {String} control.type - Type of item.
         * @param {String} control.category - Category of action (should be 'add').
         */
        function _addJointProcess (ev, control) {
            mode = 'addingJoint1';

            editor3dContainerEl.style.cursor = config.constants.CURSOR_ADD;
        }

        /**
         * Adds various listeners.
         *
         * @private
         * @memberof Editor3d
         */
        function _addListeners () {
            var controlsEventType;

            window.addEventListener('resize', _onWindowResize, false);
            window.addEventListener('mouseup', _onWindowMouseUp, false);
            window.addEventListener('keyup', _onWindowKeyUp, false);
            window.addEventListener('keydown', _onWindowKeyDown, false);

            if (config.mode.embed) {
                window.addEventListener('mousemove', _onMouseMove, false);
                window.addEventListener('mousedown', _onMouseDown, false);
            } else {
                $('#toggleAxes').on('change', function () {
                    axes.visible = $(this)[0].checked;
                })[0].checked = config.values.axes;

                $('#toggleLocalAxes').on('change', function () {
                    config.values.localAxes = $(this)[0].checked;
                    _toggleLocalAxes($(this)[0].checked);
                })[0].checked = config.values.localAxes;

                $('#toggleAutoRotate').on('change', function () {
                    controls.autoRotate = $(this)[0].checked;
                })[0].checked = config.values.autorotate;
                $('#toggleGrid').on('change', function () {
                    grid.visible = $(this)[0].checked;
                })[0].checked = config.values.grid;
                $('#toggleWireframe').on('change', function () {
                    _toggleWireframe($(this)[0].checked);
                })[0].checked = config.values.wireframe;

                editor3dEl.addEventListener('mousemove', _onMouseMove, false);
                editor3dEl.addEventListener('mousedown', _onMouseDown, false);

                $editorContextMenuEl.on('keyup', 'input', function (ev) {
                    _onContextMenuInputKeyup($(this), ev);
                });

                for (var i = 0, len = editorControlsEl.length; i < len; i++) {
                    controlsEventType = Utils.getDataAttribute(editorControlsEl[i], 'eventType') || 'click';
                    editorControlsEl[i].addEventListener(controlsEventType, _manageEditorControls, false);
                }
            }

            Body.on('change', _onBodyChange);
        }

        /**
         * Adds placeholder item to scene.
         *
         * @private
         * @memberof Editor3d
         * @param {Object} ev - Onclick event.
         * @param {Object} control - Control object with information about placeholder item.
         * @param {String} control.type - Type of item.
         * @param {String} control.category - Category of action (should be 'add').
         */
        function _addPlaceholderItem (ev, control) {
            var predictedData = Body.getNextPredictedData('p'),
                settings = {
                    line: predictedData.nextLine,
                    placeholder: true,
                    refNumber: predictedData.nextRefNumber,
                    type: 'p'
                };
            var vector, raycaster;

            vector = new THREE.Vector3(mouse.x, mouse.y, 0.5).unproject(camera);
            raycaster = new THREE.Raycaster(camera.position, vector.sub(camera.position).normalize());

            settings.code = 'p: ' + raycaster.ray.origin.x + ', ' + raycaster.ray.origin.y + ', ' + raycaster.ray.origin.z;

            mode = 'addingPart';

            if (Body.objects[Body.objects.length - 1].type === '#' && Body.objects[Body.objects.length - 1].value === '' && Body.objects[Body.objects.length - 1].refNumber !== 0) {
                Body.removeObjectByLine(Body.objects[Body.objects.length - 1].line);
                settings.line--;
            }
            Body.addPart(settings);
            Body.triggerEvent('change', _onBodyChange);

            controls.enabled = false;

            SELECTED = Body.get3dObjectByLine(settings.line);

            planeMoveHelper.position.copy(new THREE.Vector3(0, 0, 0));
            planeMoveHelper.lookAt(camera.position);

            offset = new THREE.Vector3(0, 0, 0);

            editor3dContainerEl.style.cursor = config.constants.CURSOR_MOVE;

            _centerView();
        }

        /**
         * Animates scene.
         *
         * @private
         * @memberof Editor3d
         */
        function _animate() {
            requestAnimationFrame(_animate);

            _render();
        }

        /**
         * Inits editor (scene, camera, controls, lights, objects etc.).
         *
         * @private
         * @memberof Editor3d
         */
        function _init() {
            // DOM and measurements
            editor3dEl = document.getElementById('editor3d');

            if (config.mode.embed) {
                editorWidth = window.innerWidth;
                editorHeight = window.innerHeight;
            } else {
                editor3dContentEl = document.getElementById('editor3d-content');
                editor3dContainerEl = document.getElementById('editor3d-container');
                editorWidth = editor3dContainerEl.offsetWidth;
                editorHeight = window.innerHeight - $('#top').outerHeight(true) - $('#editor3d-title').outerHeight(true) - $('#editor3d-toolbar').outerHeight(true);
            }

            editorControlsEl = document.querySelectorAll('.js-editor-controls');
            $editorContextMenuEl = $('#editor-context-menu');
            $hoverTooltipEl = $('#editor-hover-tooltip');

            // scene, raycaster, camera, controls
            scene = new THREE.Scene();
            window.scene = scene;
            raycaster = new THREE.Raycaster();
            camera = Camera.createDefaultCamera(editorWidth, editorHeight);
            controls = Controls.createDefaultControls(camera, editor3dContentEl);
            planeMoveHelper = Controls.addPlaneMoveHelperToScene(scene);
            grid = Controls.addGridHelperToScene(scene);

            window.controls = controls;

            // lights, debug axes
            Lights.addDefaultLightsToScene(scene);
            axes = Axis.addAxesToScene(scene);

            // model
            model = ModelGroup.addModelGroupToScene(scene);
            model.name = 'model';

            // placeholder item for adding objects
            placeholderItem = new THREE.Group();
            scene.add(placeholderItem);

            // add listeners
            _addListeners();

            // renderer
            renderer = new THREE.WebGLRenderer({
                canvas: editor3dEl,
                antialias: true
            });
            renderer.setSize(editorWidth, editorHeight);
            renderer.setClearColor(config.values.rendererBg, 1.0);

            // custom settings for embed
            if (config.mode.embed) {
                $embedSettingsEl = $('#embed-settings');

                if (window.FREDJS_EMBED_SHOW_AXES !== null) {
                    axes.visible = Utils.convertAttrValueToBool(window.FREDJS_EMBED_SHOW_AXES) || false;
                } else {
                    axes.visible = !!$embedSettingsEl.data('showAxes');
                }

                if (window.FREDJS_EMBED_SHOW_LOCAL_AXES !== null) {
                    config.values.localAxes = Utils.convertAttrValueToBool(window.FREDJS_EMBED_SHOW_LOCAL_AXES)|| false;
                } else {
                    config.values.localAxes = !!$embedSettingsEl.data('showLocalAxes');
                }

                if (window.FREDJS_EMBED_AUTOROTATE !== null) {
                    controls.autoRotate = Utils.convertAttrValueToBool(window.FREDJS_EMBED_AUTOROTATE) || false;
                } else {
                    controls.autoRotate = !!$embedSettingsEl.data('autorotate');
                }

                if (window.FREDJS_EMBED_SHOW_GRID !== null) {
                    grid.visible = typeof Utils.convertAttrValueToBool(window.FREDJS_EMBED_SHOW_GRID) !== 'undefined' ? Utils.convertAttrValueToBool(window.FREDJS_EMBED_SHOW_GRID) : true;
                } else {
                    grid.visible = !!$embedSettingsEl.data('showGrid');
                }

                if (window.FREDJS_EMBED_SHOW_WIREFRAME !== null) {
                    _toggleWireframe(Utils.convertAttrValueToBool(window.FREDJS_EMBED_SHOW_WIREFRAME) || false);
                } else {
                    _toggleWireframe(!!$embedSettingsEl.data('wireframe'));
                }
            }

            _animate();
        }

        /**
         * Manage editor's controls on page.
         *
         * @private
         * @memberof Editor3d
         * @param {Object} ev - Onclick event.
         */
        function _manageEditorControls (ev) {
            var target = ev.target,
                control;

            if (target.dataset.itemType === undefined && target.dataset.controlCategory === undefined) {
                target = target.parentNode;
            }

            control = {
                type: target.dataset.itemType,
                category: target.dataset.controlCategory
            };

            if (control.category === 'add') {
                switch (control.type) {
                    case 'part':
                        _addPlaceholderItem(ev, control);
                        break;
                    case 'joint':
                        _addJointProcess(ev, control);
                        break;
                    default:
                        break;
                }
            } else if (control.category === 'remove') {
                mode = 'removingItem';
            } else if (control.category === 'view') {
                switch (control.type) {
                    case 'viewUp':
                        controls.rotateUp(0.1);
                        break;
                    case 'viewDown':
                        controls.rotateUp(-0.1);
                        break;
                    case 'viewLeft':
                        controls.rotateLeft(0.1);
                        break;
                    case 'viewRight':
                        controls.rotateLeft(-0.1);
                        break;
                    case 'zoomIn':
                        controls.dollyOut();
                        break;
                    case 'zoomOut':
                        controls.dollyIn();
                        break;
                    case 'reset':
                        controls.reset();
                        break;
                    default:
                        break;
                }
            }
        }

        /**
         * Manages objects on scene
         *
         * @private
         * @memberof Editor3d
         * @param {Object} settings - Settings for object.
         */
        function _manageObject (settings) {
            var object = Body.getObjectByLine(settings.line),
                model3d = Body.get3dObjectByLine(settings.line);

            if (model3d === null) {
                switch (settings.type) {
                    case 'p':
                        if (object.model.sh.value === 2) {
                            Box.addBoxToScene(model, settings);
                        } else if (object.model.sh.value === 3) {
                            Cylinder.addCylinderAsPartToScene(model, settings);
                        } else {
                            Sphere.addSphereToScene(model, settings);
                        }
                        break;
                    case 'j':   Cylinder.addCylinderToScene(model, settings);   break;
                    default :                                                   break;
                }
            } else {
                switch (settings.type) {
                    case 'p':
                        if (object.model.sh.value <= 1) {
                            if (model3d.geometry.type !== 'SphereGeometry') {
                                Body.removeObjectFromSceneByLine(settings.line, object.model3d.parent);
                                Sphere.addSphereToScene(model, settings);
                            } else {
                                Sphere.editSphereOnScene(model, settings);
                            }
                        } else if (object.model.sh.value === 2) {
                            if (model3d.geometry.type !== 'BoxGeometry') {
                                Body.removeObjectFromSceneByLine(settings.line, object.model3d.parent);
                                Box.addBoxToScene(model, settings);
                            } else {
                                Box.editBoxOnScene(model, settings);
                            }
                        } else if (object.model.sh.value === 3) {
                            if (model3d.geometry.type !== 'CylinderGeometry') {
                                Body.removeObjectFromSceneByLine(settings.line, object.model3d.parent);
                                Cylinder.addCylinderAsPartToScene(model, settings);
                            } else {
                                Cylinder.editCylinderOnScene(model, settings);
                            }
                        }
                        break;
                    case 'j':   Cylinder.editCylinderOnScene(model, settings);  break;
                    default :                                                   break;
                }
            }
        }

        /**
         * Reacts on every Model-Body change.
         * Refreshes render view.
         *
         * @private
         * @memberof Editor3d
         */
        function _onBodyChange () {
            //console.log('zmienił się model (kod), renderuj');
            var settings;

            for (var i = 0; i < Body.objects.length; i++) {
                settings = {
                    type: Body.objects[i].type,
                    line: Body.objects[i].line
                };

                _manageObject(settings);
            }

            if (!config.mode.embed || !window.isCameraOrTargetSet()) {
                _centerView();
            }
        }

        function _centerView() {
            var box = new THREE.Box3().setFromObject(model),
                center = new THREE.Vector3((box.max.x + box.min.x)/2, (box.max.y + box.min.y)/2, (box.max.z + box.min.z)/2),
                distance;

            if ((typeof mode === 'undefined' || mode === 'idle') && isFinite(box.max.x) && isFinite(box.max.y) && isFinite(box.max.z)) {
                distance = box.max.y + (Math.abs(box.max.z) + Math.abs(box.min.z)) * .8;

                controls.target = center;
                camera.position.z = center.z + .5;
                camera.position.x = center.x + .5;
                camera.position.y = distance < 3 ? 3 : distance;
            }
        }

        function _onContextMenuInputKeyup (el, ev) {
            var object = Body.getObjectByLine(el.data('modelLine'));

            if (ev.keyCode !== 9 && object !== null && object.model[el.data('propName')] !== undefined) {
                object.model[el.data('propName')].value = el.val();
                object.model[el.data('propName')].ommit = false;
                object.model[el.data('propName')].shortNotation = false;

                Body.triggerEvent('change', _onBodyChange);
            }
        }

        /**
         * Reacts on mousedown event.
         *
         * @private
         * @memberof Editor3d
         * @param {Object} ev - MouseDown event.
         */
        function _onMouseDown (ev) {
            var vector, raycaster, intersects,
                frontIntersected, object,
                isRightMB;

            if (controls.autoRotate && !config.mode.embed) {
                controls.autoRotate = false;
                $('#toggleAutoRotate')[0].checked = false;
            }

            if (ev.which) {
                // Gecko (Firefox), WebKit (Safari/Chrome) & Opera
                isRightMB = (ev.which === 3);
            } else if (ev.button) {
                // IE, Opera
                isRightMB = (ev.button === 2);
            }

            ev.preventDefault();

            vector = new THREE.Vector3(mouse.x, mouse.y, 0.5).unproject(camera);
            raycaster = new THREE.Raycaster(camera.position, vector.sub(camera.position).normalize());
            intersects = raycaster.intersectObjects(model.children);

            $editorContextMenuEl.hide();

            if (intersects.length > 0 && !config.mode.embed) {
                frontIntersected = intersects[0];

                if (isRightMB) {
                    object = Body.getObjectByLine(frontIntersected.object.line);

                    $hoverTooltipEl.hide();
                    $editorContextMenuEl.html(object.generateContextMenu());
                    $editorContextMenuEl.css({
                        top: ev.clientY + 15,
                        left: ev.clientX + 15
                    });
                    $editorContextMenuEl.show();

                    SELECTED = frontIntersected.object;
                    mode = 'contextMenu';
                }

                if (mode !== 'removingItem') {
                    if (frontIntersected.object.isDraggable) {
                        controls.enabled = false;

                        SELECTED = frontIntersected.object;
                        intersects = raycaster.intersectObject(planeMoveHelper);

                        if (mode !== 'addingPart') {
                            offset.copy(frontIntersected.point).sub(planeMoveHelper.position);
                        }

                        editor3dContainerEl.style.cursor = config.constants.CURSOR_MOVE;
                    } else {
                        controls.enabled = true;
                    }
                }

                if (mode === 'addingJoint1') {
                    editor3dContainerEl.style.cursor = config.constants.CURSOR_ADD;
                } else if (mode === 'removingItem') {
                    if (confirm('Are you sure you want to remove this object?')) {
                        _removeItem(frontIntersected);
                    }
                    controls.enabled = false;
                    mode = 'idle';
                }
            } else {
                if (mode === 'removingItem' || mode === 'contextMenu') {
                    mode = 'idle';

                    if (SELECTED !== null) {
                        object = Body.getObjectByLine(SELECTED.line);
                        SELECTED.material.color.setRGB(object.model.vr.value, object.model.vg.value, object.model.vb.value);
                        SELECTED = null;
                    }
                }
                controls.enabled = true;
            }
        }

        /**
         * Reacts on mousemove event.
         *
         * @private
         * @memberof Editor3d
         * @param {Object} ev - MouseMove event.
         */
        function _onMouseMove(ev) {
            var intersects,
                intersectedPosition,
                object;

            ev.preventDefault();

            mouse.x = 2 * (ev.offsetX / editorWidth) - 1;
            mouse.y = 1 - 2 * (ev.offsetY / editorHeight);

            raycaster.setFromCamera(mouse, camera);

            if (SELECTED && !config.mode.embed) {     // move selected object
                if (mode === 'contextMenu' || mode === 'addingJoint1' || mode === 'addingJoint2') {
                    return;
                }
                $hoverTooltipEl.hide();

                intersects = raycaster.intersectObject(planeMoveHelper);

                object = Body.getObjectByLine(SELECTED.line);

                intersectedPosition = intersects[0].point.sub(offset);
                intersectedPosition.x = parseFloat(intersectedPosition.x.toFixed(2));
                intersectedPosition.y = parseFloat(intersectedPosition.y.toFixed(2));
                intersectedPosition.z = parseFloat(intersectedPosition.z.toFixed(2));

                if (keyControls.x) {
                    object.model.x.value = intersectedPosition.x;
                    object.model.x.ommit = false;
                }
                if (keyControls.c) {
                    object.model.y.value = intersectedPosition.y;
                    object.model.y.ommit = false;
                }
                if (keyControls.z) {
                    object.model.z.value = intersectedPosition.z;
                    object.model.z.ommit = false;
                }

                if (!keyControls.x && !keyControls.c && !keyControls.z) {
                    object.model.x.value = intersectedPosition.x;
                    object.model.y.value = intersectedPosition.y;
                    object.model.z.value = intersectedPosition.z;
                    object.model.x.ommit = false;
                    object.model.y.ommit = false;
                    object.model.z.ommit = false;
                }

                SELECTED.position.copy(intersectedPosition);

                if (mode !== 'addingPart' && mode !== 'addingJoint1' && mode !== 'addingJoint2') {
                    mode = 'moveObject';
                }

                Body.triggerEvent('change', _onBodyChange);

                return;
            }

            intersects = raycaster.intersectObjects(model.children);

            if (intersects.length > 0 && !config.mode.embed) {

                if (INTERSECTED !== intersects[0].object) { //

                    if (INTERSECTED) {
                        object = Body.getObjectByLine(INTERSECTED.line);
                        INTERSECTED.material.color.setRGB(object.model.vr.value, object.model.vg.value, object.model.vb.value);
                    }

                    INTERSECTED = intersects[0].object;
                    object = Body.getObjectByLine(INTERSECTED.line);

                    if (controls.custom.state === -1) {
                        if (mode === 'contextMenu' || object.type === 'p' || object.type === 'j') {
                            INTERSECTED.material.color.setStyle(config.values.hoverColor);
                        }

                        /*editor.getSession().setAnnotations([{
                            row: INTERSECTED.line,
                            column: 1,
                            text: 'Last hovered element',
                            type: 'info'
                        }]);*/

                        $hoverTooltipEl.html(object.generateHoverTooltip());
                        $hoverTooltipEl.css({
                            top: ev.clientY + 15,
                            left: ev.clientX + 15
                        });
                        $hoverTooltipEl.show();
                    }

                    planeMoveHelper.position.copy(INTERSECTED.position);
                    planeMoveHelper.lookAt(camera.position);
                }

                if (controls.custom.state === -1) {
                    editor3dContainerEl.style.cursor = config.constants.CURSOR_POINTER;
                }

            } else {
                /*if (!config.mode.embed && typeof mode !== 'undefined') {
                    editor.getSession().clearAnnotations();
                }*/
                $hoverTooltipEl.hide();

                if (INTERSECTED) {
                    object = Body.getObjectByLine(INTERSECTED.line);

                    if (object !== jointProcess.p1 && object !== jointProcess.p2 && typeof object.model !== 'undefined' &&
                        typeof object.model.vr !== 'undefined' && typeof object.model.vg !== 'undefined' && typeof object.model.vb !== 'undefined') {
                        INTERSECTED.material.color.setRGB(object.model.vr.value, object.model.vg.value, object.model.vb.value);
                    }
                }

                INTERSECTED = null;

                if (!config.mode.embed) {
                    if (mode === 'addingJoint1' || mode === 'addingJoint2' || mode === 'removingItem') {
                        editor3dContainerEl.style.cursor = config.constants.CURSOR_ADD;
                    } else {
                        editor3dContainerEl.style.cursor = config.constants.CURSOR_AUTO;
                    }
                }
            }
        }

        /**
         * Reacts on keyup event.
         *
         * @private
         * @memberof Editor3d
         * @param {Object} ev - MouseDown event.
         */
        function _onWindowKeyDown (ev) {
            switch (ev.keyCode) {
                case 67: keyControls.c = true; break;
                case 88: keyControls.x = true; break;
                case 90: keyControls.z = true; break;
                default:
                    break;
            }
        }

        /**
         * Reacts on keyup event.
         *
         * @private
         * @memberof Editor3d
         * @param {Object} ev - KeyUp event.
         */
        function _onWindowKeyUp (ev) {
            switch (ev.keyCode) {
                case 27: // ESC
                    if (mode === 'addingJoint2') {
                        jointProcess.p1.model3d.material.color.setRGB(jointProcess.p1.model.vr.value, jointProcess.p1.model.vg.value, jointProcess.p1.model.vb.value);
                        jointProcess = {
                            p1: null,
                            p2: null
                        };
                    } else if (mode === 'addingPart') {
                        Body.removeObjectByLine(SELECTED.line, true);
                        Body.triggerEvent('change', _onBodyChange);
                        SELECTED = null;
                    }

                    if (mode !== 'contextMenu') {
                        mode = 'idle';
                    }

                    break;
                case 67: keyControls.c = false; break;
                case 88: keyControls.x = false; break;
                case 90: keyControls.z = false; break;
                default:
                    break;
            }
        }


        /**
         * Reacts on mouseup event.
         *
         * @private
         * @memberof Editor3d
         * @param {Object} ev - MouseUp event.
         */
        function _onWindowMouseUp (ev) {
            ev.preventDefault();

            if (mode !== 'contextMenu') {
                if (INTERSECTED) {
                    _addJoint(INTERSECTED);

                    planeMoveHelper.position.copy( INTERSECTED.position );
                    SELECTED = null;
                } else if (mode === 'addingPart') {
                    planeMoveHelper.position.copy( SELECTED.position );
                    SELECTED = null;
                    mode = 'idle';
                } else if (mode === 'moveObject') {
                    mode = 'idle';
                }
            }

            if (!config.mode.embed) {
                editor3dContainerEl.style.cursor = 'auto';
            }
        }

        /**
         * Reacts on window resize event.
         *
         * @private
         * @memberof Editor3d
         */
        function _onWindowResize () {
            if (renderer) {
                if (config.mode.embed) {
                    editorWidth = window.innerWidth;
                    editorHeight = window.innerHeight;
                } else {
                    editor3dContainerEl = document.getElementById('editor3d-container');
                    editorWidth = editor3dContainerEl.offsetWidth;
                    editorHeight = window.innerHeight - $('#top').outerHeight(true) - $('#editor3d-title').outerHeight(true) - $('#editor3d-toolbar').outerHeight(true);
                }

                camera.aspect = editorWidth / editorHeight;
                camera.updateProjectionMatrix();

                renderer.setSize(editorWidth, editorHeight);
            }
        }

        function _removeItem(frontIntersected) {
            var object = Body.getObjectByLine(frontIntersected.object.line),
                joints;

            if (object.type === 'p') {
                joints = Body.getJointsByPartRefNumber(object.refNumber);
                if (joints.length > 0) {
                    for (var i = 0, len = joints.length; i< len; i++) {
                        Body.removeObjectByLine(joints[i].line, true);
                    }
                }
            }

            Body.removeObjectByLine(frontIntersected.object.line, true);
            Body.triggerEvent('change', _onBodyChange);
        }

        /**
         * Renders scene.
         *
         * @private
         * @memberof Editor3d
         */
        function _render() {
            controls.update();

            renderer.render(scene, camera);
        }

        function _toggleLocalAxes (value) {
            for (var i = 0, len = Body.objects.length; i < len; i++) {
                if (Body.objects[i].type === 'p' && typeof Body.objects[i].localAxes !== 'undefined') {
                    Body.objects[i].localAxes.visible = value;
                }
            }
        }

        /**
         * Toggles wireframe.
         *
         * @param {Boolean} value
         * @private
         */
        function _toggleWireframe (value) {
            var obj;
            config.values.wireframe = value;

            for (var i = 0, len = Body.objects.length; i < len; i++) {
                obj = Body.objects[i];

                if (obj.model3d) {
                    obj.model3d.material.wireframe = value;
                }
            }
        }

        return Editor3d;
    }
);