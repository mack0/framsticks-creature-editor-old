/**
 *  A module representing Sphere.
 *  @namespace Editor3d/Scene
 */
define(
    'Editor3d/Scene/Sphere/sphere-module',
    [
        'THREE',
        'Editor3d/Scene/Axis/axis-module',
        'Configuration/configuration-module',
        'Utils/utils-module',

        'Model/Body'
    ],
    function SphereModule(THREE, Axis, config, Utils, Body) {
        "use strict";

        /////////////////// API
        /**
         * @class
         * @memberof Sphere
         */
        var Sphere = {
            addSphereToScene: addSphereToScene,
            editSphereOnScene: editSphereOnScene
        };

        /////////////////// Methods
        /**
         * Adds Sphere to scene with given settings.
         *
         * @memberof Sphere
         * @param {Object} model - Group with 3d model on scene.
         * @param {Object} settings - Settings for new Sphere.
         */
        function addSphereToScene(model, settings) {
            var object = Body.getObjectByLine(settings.line),
                geometry, material, mesh;

            geometry = new THREE.SphereGeometry(1, 16, 16);

            material = new THREE.MeshLambertMaterial({color: config.values.partColor, wireframe: config.values.wireframe});
            material.color.setRGB(object.model.vr.value, object.model.vg.value, object.model.vb.value);

            mesh = new THREE.Mesh(geometry, material);
            mesh.overdraw = true;

            if (object.model.sh.value === 1) {
                mesh.scale.set(object.model.sx.value, object.model.sy.value, object.model.sz.value);
            } else {
                mesh.scale.set(config.values.part.radius, config.values.part.radius, config.values.part.radius);
            }

            if (!isNaN(object.model.rx.value) && !isNaN(object.model.ry.value) && !isNaN(object.model.rz.value)) {
                mesh.rotation.order = 'ZYX'; //follow the way Framsticks rotates Parts
                mesh.rotation.x = object.model.rx.value;
                mesh.rotation.y = -object.model.ry.value;
                mesh.rotation.z = object.model.rz.value;
            }

            mesh.position.x = object.model.x.value;
            mesh.position.y = object.model.y.value;
            mesh.position.z = object.model.z.value;

            // Fred specific properites
            mesh.f0type = 'p';
            mesh.line = settings.line;
            mesh.isDraggable = true;

            object.add3dRepresentation(mesh);
            model.add(mesh);

            if (typeof object.localAxes === 'undefined') {
                object.localAxes = Axis.addLocalAxesToObject(object.model3d);
                object.localAxes.visible = config.values.localAxes;
            }
        }

        /**
         * Edits Sphere with given settings.
         *
         * @memberof Sphere
         * @param {Object} model - Group with 3d model on scene.
         * @param {Object} settings - Settings for edited Sphere.
         */
        function editSphereOnScene(model, settings) {
            var object = Body.getObjectByLine(settings.line),
                object3d = Body.get3dObjectByLine(settings.line);

            object.localAxes.visible = config.values.localAxes;

            if (object.model.sh.value === 1) {
                object3d.scale.set(object.model.sx.value, object.model.sy.value, object.model.sz.value);
            } else if (object.model.sh.value === 0) {
                object3d.scale.set(config.values.part.radius, config.values.part.radius, config.values.part.radius);
            }

            object3d.rotation.order = 'ZYX'; //follow the way Framsticks rotates Parts
            object3d.rotation.x = object.model.rx.value;
            object3d.rotation.y = -object.model.ry.value;
            object3d.rotation.z = object.model.rz.value;

            object3d.position.x = object.model.x.value;
            object3d.position.y = object.model.y.value;
            object3d.position.z = object.model.z.value;

            object3d.material.color.setRGB(object.model.vr.value, object.model.vg.value, object.model.vb.value);
        }

        return Sphere;
    }
);