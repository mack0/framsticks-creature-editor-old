/**
 *  A module representing Box.
 *  @namespace Editor3d/Scene
 */
define(
    'Editor3d/Scene/Box/box-module',
    [
        'THREE',
        'Editor3d/Scene/Axis/axis-module',
        'Configuration/configuration-module',
        'Utils/utils-module',

        'Model/Body'
    ],
    function BoxModule(THREE, Axis, config, Utils, Body) {
        "use strict";

        /////////////////// API
        /**
         * @class
         * @memberof Box
         */
        var Box = {
            addBoxToScene: addBoxToScene,
            editBoxOnScene: editBoxOnScene
        };

        /////////////////// Methods
        /**
         * Adds Box to scene with given settings.
         *
         * @memberof Box
         * @param {Object} model - Group with 3d model on scene.
         * @param {Object} settings - Settings for new Box.
         */
        function addBoxToScene(model, settings) {
            var object = Body.getObjectByLine(settings.line),
                geometry, material, mesh;

            geometry = new THREE.BoxGeometry(2, 2, 2);

            material = new THREE.MeshLambertMaterial({color: config.values.partColor, wireframe: config.values.wireframe});
            material.color.setRGB(object.model.vr.value, object.model.vg.value, object.model.vb.value);

            mesh = new THREE.Mesh(geometry, material);
            mesh.overdraw = true;

            mesh.scale.set(object.model.sx.value, object.model.sy.value, object.model.sz.value);

            if (!isNaN(object.model.rx.value) && !isNaN(object.model.ry.value) && !isNaN(object.model.rz.value)) {
                mesh.rotation.order = 'ZYX'; //follow the way Framsticks rotates Parts
                mesh.rotation.x = object.model.rx.value;
                mesh.rotation.y = -object.model.ry.value;
                mesh.rotation.z = object.model.rz.value;
            }

            mesh.position.x = object.model.x.value;
            mesh.position.y = object.model.y.value;
            mesh.position.z = object.model.z.value;

            // Fred specific properites
            mesh.f0type = 'p';
            mesh.line = settings.line;
            mesh.isDraggable = true;

            object.add3dRepresentation(mesh);
            model.add(mesh);

            if (typeof object.localAxes === 'undefined') {
                object.localAxes = Axis.addLocalAxesToObject(object.model3d);
                object.localAxes.visible = config.values.localAxes;
            } else {
                object.localAxes.visible = config.values.localAxes;
            }
        }

        /**
         * Edits Box with given settings.
         *
         * @memberof Box
         * @param {Object} model - Group with 3d model on scene.
         * @param {Object} settings - Settings for edited Box.
         */
        function editBoxOnScene(model, settings) {
            var object = Body.getObjectByLine(settings.line),
                object3d = Body.get3dObjectByLine(settings.line);

            object.localAxes.visible = config.values.localAxes;

            object3d.scale.set(object.model.sx.value, object.model.sy.value, object.model.sz.value);

            object3d.rotation.order = 'ZYX'; //follow the way Framsticks rotates Parts
            object3d.rotation.x = object.model.rx.value;
            object3d.rotation.y = -object.model.ry.value;
            object3d.rotation.z = object.model.rz.value;

            object3d.position.x = object.model.x.value;
            object3d.position.y = object.model.y.value;
            object3d.position.z = object.model.z.value;

            object3d.material.color.setRGB(object.model.vr.value, object.model.vg.value, object.model.vb.value);
        }

        return Box;
    }
);