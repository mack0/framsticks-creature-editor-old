/**
 *  A module representing Axis.
 *  @namespace Editor3d/Scene
 */
define(
    'Editor3d/Scene/Axis/axis-module',
    ['THREE', 'Configuration/configuration-module'],
    function AxisModule (THREE, config) {
        "use strict";

        /////////////////// API
        /**
         * @class
         * @memberof Axis
         */
        var Axis = {
            addAxesToScene: addAxesToScene,
            addLocalAxesToObject: addLocalAxesToObject
        };

        /////////////////// Private Methods
        /**
         * Build and returns Axes.
         *
         * @private
         * @memberof Axis
         * @param {Number} length - Length of Axis.
         * @param {Object} [position] - Position of axes
         * @return {Array}
         */
        function _buildAxes (length, position) {
            var axes = new THREE.Object3D();

            position = position || new THREE.Vector3( 0, 0, 0 );

            /* colors and stroke style:
                +X -> red   solid   0 -> +Inf
                -X -> red   dashed  0 -> -Inf

                +Y -> green solid   0 -> +Inf
                -Y -> green dashed  0 -> -Inf

                +Z -> blue  solid   0 -> +Inf
                -Z -> blue  dashed  0 -> -Inf
             */

            axes.add(_buildAxis( position, new THREE.Vector3( length, 0, 0 ), 0xFF0000, false ) ); // +X
            axes.add(_buildAxis( position, new THREE.Vector3( -length, 0, 0 ), 0xFF0000, true) ); // -X
            axes.add(_buildAxis( position, new THREE.Vector3( 0, length, 0 ), 0x00FF00, false ) ); // +Y
            axes.add(_buildAxis( position, new THREE.Vector3( 0, -length, 0 ), 0x00FF00, true ) ); // -Y
            axes.add(_buildAxis( position, new THREE.Vector3( 0, 0, length ), 0x0000FF, false ) ); // +Z
            axes.add(_buildAxis( position, new THREE.Vector3( 0, 0, -length ), 0x0000FF, true ) ); // -Z

            return axes;
        }

        /**
         * Build and returns Axis.
         *
         * @private
         * @memberof Axis
         * @param {Object} src - Source of Axis.
         * @param {Object} dst - Destination for new Axis.
         * @param {String} colorHex - HEX representation of color.
         * @param {Boolean} dashed - Determines if Axis line should be dashed.
         * @return {Object}
         */
        function _buildAxis (src, dst, colorHex, dashed) {
            var geom = new THREE.Geometry(),
                mat, axis;

            if (dashed) {
                mat = new THREE.LineDashedMaterial({ linewidth: 2, color: colorHex, dashSize: 0.5, gapSize: 0.5 });
            } else {
                mat = new THREE.LineBasicMaterial({ linewidth: 2, color: colorHex });
            }

            geom.vertices.push( src.clone() );
            geom.vertices.push( dst.clone() );
            geom.computeLineDistances();

            axis = new THREE.Line( geom, mat, THREE.LinePieces );

            return axis;
        }

        /////////////////// Methods
        /**
         * Adds Axes to scene.
         *
         * @memberof Axis
         * @param {Object} scene - Scene.
         * @return {Array}
         */
        function addAxesToScene (scene) {
            var axes = _buildAxes(10);
            axes.visible = config.values.axes;
            scene.add(axes);
            return axes;
        }

        /**
         * Adds local Axes to object.
         *
         * @memberof Axis
         * @param {Object} object
         * @param {Object} [position]
         * @return {Array}
         */
        function addLocalAxesToObject (object, position) {
            var axes = _buildAxes(4, position);
            object.add(axes);
            return axes;
        }

        return Axis;
    }
);