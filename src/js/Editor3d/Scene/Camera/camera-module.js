/**
 *  A module representing Camera.
 *  @namespace Editor3d/Scene
 */
define(
    'Editor3d/Scene/Camera/camera-module',
    ['THREE', 'Configuration/configuration-module'],
    function CameraModule (THREE, config) {
        "use strict";

        /////////////////// API
        /**
         * @class
         * @memberof Camera
         */
        var Camera = {
            get camera () {
                return cameraObj;
            },
            createDefaultCamera: createDefaultCamera
        };

        var cameraObj;

        /////////////////// Methods
        /**
         * Creates defualt camera.
         *
         * @memberof Camera
         * @param {Number} width - width of container in which camera operates.
         * @param {Number} height - height of container in which camera operates.
         */
        function createDefaultCamera (width, height) {
            var camera = new THREE.PerspectiveCamera(75, width / height, 1, 10000),
                $embedSettingsEl = $('#embed-settings'),
                cameraPos = {};

            camera.up = new THREE.Vector3( 0, 0, 1 );

            // prefer given options over defaults
            cameraPos.x = (typeof window.FREDJS_EMBED_CAMERA_X !== 'undefined' && window.FREDJS_EMBED_CAMERA_X !== null) ? parseFloat(window.FREDJS_EMBED_CAMERA_X) : $embedSettingsEl.data('cameraX');
            cameraPos.y = (typeof window.FREDJS_EMBED_CAMERA_Y !== 'undefined' && window.FREDJS_EMBED_CAMERA_Y !== null) ? parseFloat(window.FREDJS_EMBED_CAMERA_Y) : $embedSettingsEl.data('cameraY');
            cameraPos.z = (typeof window.FREDJS_EMBED_CAMERA_Z !== 'undefined' && window.FREDJS_EMBED_CAMERA_Z !== null) ? parseFloat(window.FREDJS_EMBED_CAMERA_Z) : $embedSettingsEl.data('cameraZ');

            cameraPos.x = typeof cameraPos.x !== 'undefined' ? cameraPos.x : config.values.camera.position.x;
            cameraPos.y = typeof cameraPos.y !== 'undefined' ? cameraPos.y : config.values.camera.position.y;
            cameraPos.z = typeof cameraPos.z !== 'undefined' ? cameraPos.z : config.values.camera.position.z;

            camera.position.z = cameraPos.z;
            camera.position.y = cameraPos.y;
            camera.position.x = cameraPos.x;

            window.camera = camera;

            cameraObj = camera;

            return camera;
        }

        return Camera;
    }
);