# FredJS
[Framsticks](http://www.framsticks.com/) creature editor.

## Environment:
Dependencies:

 - NodeJS (tested with version 0.12.7)
 - npm (tested with version 2.11.3)
 - bower (tested with version 1.4.1)
 - gulp (tested with version 3.9.0)

## Build process:
 1. npm install
 2. bower install
 3. gulp build
 
## Development
After the build process is complete, you can use the default gulp task: ```gulp```. 
It creates a development server for you and synchronizes actions between currently open browsers.