'use strict';

// simple express server
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var router = express.Router();
var mongoose = require('mongoose');
var db = mongoose.connection;
var ObjectId = mongoose.Types.ObjectId;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

mongoose.connect('mongodb://localhost/fred');

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log('connection established');
});

var modelSchema = mongoose.Schema({
    code: String,
    name: String
});

var Model = mongoose.model('Model', modelSchema);

app.use(express.static('build'));
app.get('/', function(req, res) {
    res.sendfile('./build/index.html');
});
app.get('/api/models', function(req, res) {
    Model.find(function (err, models) {
        if (err) return console.error(err);
        console.log(models);

        res.status(200).json(models);
    });
});

app.get('/api/models/:id', function(req, res) {
    console.log('model id:', req.params.id);

    Model.findById(req.params.id, function (err, model) {
        if (err) {
            res.status(400).json({});
        }

        res.status(200).json(model);
    });
});

app.put('/api/models/:id', function(req, res) {
    console.log('req.body', req.body);

    Model.findByIdAndUpdate(req.params.id, req.body, function (err, model) {
        if (err) {
            res.status(400).json({});
        }

        res.status(200).json(model);
    });
});

app.post('/api/models', function(req, res) {
    console.log('req.body', req.body);

    Model.create(req.body, function (err, model) {
        if (err) {
            res.status(400).json({});
        }

        res.status(200).json(model);
    });
});

app.listen(5000);